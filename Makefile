# Copyright 2024 pan
# SPDX-License-Identifier: GPL-2.0-or-later

CC := gcc
CFLAGS := \
	-g0 \
	-O3 \
	-T script.ld \
	-DNDEBUG \
	-march=native \
	-mtune=native \
	-fdata-sections \
	-ffunction-sections \
	-ffreestanding \
	-fno-asynchronous-unwind-tables \
	-fno-ident \
	-fno-plt \
	-fno-pie \
	-fno-pic \
	-fcf-protection=none \
	-mmanual-endbr \
	-ffat-lto-objects \
	-flto \
	-nostdlib \
	-nostartfiles \
	-nodefaultlibs \
	-nolibc \
	-std=gnu18 \
	-Wall \
	-Wextra \
	-Wpedantic

LDFLAGS := \
	-Wl,--gc-sections \
	-Wl,-O1 \
	-Wl,--build-id=none \
	-Wl,--as-needed \
	-Wl,--strip-all \
	-Wl,--strip-debug \
	-Wl,-z,max-page-size=0x800 \
	-Wl,-z,noseparate-code

STRIP := -s

.PHONY: all sstrip

all: rot13 rot47

C = \
	@$(CC) -static $(CFLAGS) $(STRIP) -no-pie $< -o $@ $(LDFLAGS) && \
	 objcopy --remove-section='.*' --remove-section='!.text' $@ && \
	 strip --strip-all $@ && \
	 sstrip/sstrip -z $@

rot13: rot13.c sstrip
	$(C)

rot47: rot47.c sstrip
	$(C)

sstrip:
	@$(MAKE) --no-print-directory -C $@

.PHONY: clean clean_all

clean:
	@rm -f rot13 rot47

clean_all: clean
	@$(MAKE) --no-print-directory -C sstrip clean_all
