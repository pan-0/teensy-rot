// Copyright 2024 pan
// SPDX-License-Identifier: GPL-2.0-or-later

#include <stddef.h>        /* size_t */
#include <sys/types.h>     /* ssize_t */
#include <linux/unistd.h>  /* __NR_exit, __NR_read, __NR_write */

typedef size_t  usize;
typedef ssize_t isize;

/*
 * Platform layer.
 */

enum {
	STDIN_FD  = 0,
	STDOUT_FD = 1
};

/*
 * NOTE: Bad things will happen if you use this for system calls other than
 * read(2) and write(2).
 */
inline static long syscall_3(int number, int fd, long _2, long _3)
{
	register long rax asm("rax");

	register int  eax asm("eax") = number;
	register int  edi asm("edi") = fd;
	register long rsi asm("rsi") = _2;
	register long rdx asm("rdx") = _3;

	asm volatile ("syscall"
		: /* outputs  */ "=r"(rax)
		: /* inputs   */ "r"(eax), "r"(edi), "r"(rsi), "r"(rdx)
		: /* clobbers */ "rcx", "r11", "cc", "memory"
	);

	return (unsigned long)rax > -4096ul ? -rax : rax;
}

inline static _Noreturn void xexit(int code)
{
	register int c asm("edi") = code;

	asm volatile (
		"mov %0, %%eax\n\t"
		"syscall\n\t"

		: /* outputs */
		: /* inputs  */ "i"(__NR_exit), "r"(c)
	);

	__builtin_unreachable();
}

inline static isize xread(int fd, void *buffer, usize count)
{
	return syscall_3(__NR_read, fd, (long)buffer, (long)count);
}

inline static isize xwrite(int fd, const void *buffer, usize count)
{
	return syscall_3(__NR_write, fd, (long)buffer, (long)count);
}


/*
 * ROT13
 */

/* x in [a, b] */
#define in_range(x, a, b) ((unsigned)((x) - (a)) < (b) - (a) + 1)

static
void rot47(usize size, char *buf)
{
	for (char *it = buf; it != buf + size; ++it) {
		char ch = *it;
		if (in_range(ch, '!', '~'))
            *it = (ch - '!' + 47) % 94u + '!';
	}
}

static isize qread(int fd, char *buf, usize size, usize head, usize tail)
{
	/* [ | | | | ]  [ | | | | ]
	 *   ^     ^      ^     ^
	 *   head  tail   tail  head
	 */
	usize end = (head <= tail ? size : head);
	if (end - tail == 0) return 0;

	isize r = xread(fd, buf + tail, end - tail);
	return r == 0 ? -1 : r;
}

static isize qwrite(int fd, const char *buf, usize size, usize head, usize tail)
{
	/*
	 * [ | | | | ]  [ | | | | ]
	 *   ^     ^      ^     ^
	 *   head  tail   tail  head
	 */
	usize end = (head < tail ? tail : size);
	return end - head == 0 ? 0 : xwrite(fd, buf + head, end - head);
}

static int go(void)
{
	char buf[1u << 15];  /* 32KiB */
	enum { size = sizeof buf };

	usize head = 0;
	usize tail = 0;
	while (1) {
		char *begin = buf + tail;
		isize r;

		r = qread(STDIN_FD, buf, size, head, tail);
		if (r < 0)
			break;

		tail = (tail + (usize)r) % size;
		rot47((usize)r, begin);

		r = qwrite(STDOUT_FD, buf, size, head, tail);
		if (r < 0)
			return 1;

		head = (head + (usize)r) % size;
	}

	/* TODO: Maybe this doesn't completely suffice. */
	return head != tail && qwrite(STDOUT_FD, buf, size, head, tail) < 0;
}

__attribute__((__force_align_arg_pointer__))
_Noreturn void _start(void)
{
	int r = go();
	xexit(r);
	__builtin_unreachable();
}

